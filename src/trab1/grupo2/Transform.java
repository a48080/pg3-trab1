package trab1.grupo2;

public abstract class Transform {

    private final String name;

    protected Transform(String nm) {
        this.name = nm;
    }

    public abstract String[] apply(String... l) throws TransformException;

    @Override
    public final String toString() {
        return name;
    }

    public static String getString(Transform[] tfs, String sep){
        StringBuilder aux = new StringBuilder();
        for(Transform auxtfs : tfs) {
            aux.append(auxtfs.toString()).append(sep);
        }
        if(tfs.length != 0)
            aux.delete(aux.length()-sep.length(), aux.length());
        return aux.toString();
    }
}
