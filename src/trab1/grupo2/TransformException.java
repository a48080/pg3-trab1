package trab1.grupo2;

public class TransformException extends Exception{

    private final Transform transf;

    public TransformException(Transform transform, String msg) {
        super(transform.toString() + msg);
        transf = transform;
    }

    public Transform getTransform(){
        return transf;
    }
}