package trab1.grupo2;

import java.util.Arrays;

public class Split extends Transform {

    private final char separator;

    public Split(char sep) {
        super("split");
        this.separator = sep;
    }

    @Override
    public String[] apply(String... l) {

        String sep = "\\Q" + this.separator + "\\E";

        String[] auxArray = {};

        for (String s : l) {
        String[] parcial = s.split(sep);
        String[] aux = Arrays.copyOf(auxArray,auxArray.length+parcial.length);
        System.arraycopy(parcial,0,aux,auxArray.length,parcial.length);
        auxArray = aux;
        }
        return auxArray;
    }
}
