package trab1.grupo2;


public class Seq extends Transform {

    private final Transform[] tfs;


    @Override
    public String[] apply(String... l) throws TransformException{
        String[] aux = null;
        for(Transform t: tfs){
          aux = t.apply(l);
          l = aux;
        }
        return aux;
    }

    public Seq(Transform... tfs) {
        super("sequence: " + getString(tfs," >> "));
        this.tfs = tfs;
    }

}
