package trab1.grupo2;


public class JoinPairs extends Transform {

    private final String sep;


    public JoinPairs(String sep) {
        super("join pairs with \"" + sep + "\"" );
        this.sep = sep;
    }

    @Override
    public String[] apply(String... l) throws TransformException {

        if(l.length < 2) throw new TransformException(this,"no pairs found");

        String[] aux = new String[l.length-1];

        for (int i =0; i < l.length - 1; i++) {
            aux[i] = l[i] + sep + l[i+1];
        }
        return aux;
    }
}