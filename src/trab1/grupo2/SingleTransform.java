package trab1.grupo2;


public abstract class SingleTransform extends Transform {

    public SingleTransform(String nm) {
        super(nm);
    }

    @Override
    public String[] apply(String... l) {

        String[] auxArray = new String[l.length];

        for (int i=0; i<l.length; i++) {
            auxArray[i] = applyForEach(l[i]);
        }
        return auxArray;
    }

    protected abstract String applyForEach(String s);

}
