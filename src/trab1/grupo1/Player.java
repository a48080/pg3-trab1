package trab1.grupo1;

import java.util.*;

/**
 * The class Player.
 */
public class Player implements Comparable<Player> {

    /**
     * The Name.
     */
    private final String name;

    /**
     * The Num quizzes.
     */
    private int numQuizzes;

    /**
     * The Total quiz score.
     */
    private int totalQuizPoints;

    /**
     * Instantiates a new Player.
     *
     * @param nm  the nm
     * @param nq  the nq
     * @param tqp the tqp
     */
    public Player(String nm, int nq, int tqp) {
        this.name = nm;
        this.numQuizzes = nq;
        this.totalQuizPoints = tqp;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets num quizzes.
     *
     * @return the num quizzes
     */
    public int getNumQuizzes() {
        return numQuizzes;
    }

    /**
     * Gets total quiz points.
     *
     * @return the total quiz points
     */
    public int getTotalQuizPoints() {
        return totalQuizPoints;
    }

    /**
     * Sets num quizzes.
     *
     * @param numQuizzes the num quizzes
     */
    public void setNumQuizzes(int numQuizzes) {
        this.numQuizzes = numQuizzes;
    }


    /**
     * Sets total quiz points.
     *
     * @param totalQuizScore the total quiz score
     */
    public void setTotalQuizPoints(int totalQuizScore) {
        this.totalQuizPoints = totalQuizScore;
    }

    /**
     * To string string.
     *
     * @return the string
     */
    @Override
    public String toString() {
        return numQuizzes + ":" + totalQuizPoints + " - " + name;
    }

    @Override
    public int compareTo(Player other) {
        return this.getPoints() - other.getPoints();
    }

    /**
     * Construtor com um parâmetro do tipo java.lang.String. O formato da string passada por parâmetro é
     * <toString>::= <numQuizzes>‘:’<totalQuizPoints> ‘-’ <name>
     * "5:47  - Manuel Torres"
     *
     * @param playerAsString the player as string
     */
    public Player(String playerAsString) {
        playerAsString.indexOf(':');

        int indexDoisPontos = indexCaracteresEspeciais(playerAsString, ':');
        int indexHifen = indexCaracteresEspeciais(playerAsString, '-');
        playerAsString.substring(0, indexDoisPontos);
        playerAsString.substring(indexDoisPontos + 1, indexHifen);
        playerAsString.substring(indexHifen + 1);

        this.name = playerAsString.substring(indexHifen + 1).strip();
        this.numQuizzes = Integer.parseInt(playerAsString.substring(0, indexDoisPontos).replaceAll("\\s", ""));
        this.totalQuizPoints = Integer.parseInt(playerAsString.substring(indexDoisPontos + 1, indexHifen).replaceAll("\\s", ""));

    }

    private int indexCaracteresEspeciais(String stringComoParametro, char caracterDivisor) {
        return stringComoParametro.indexOf(caracterDivisor);
    }

    public int getPoints() {
        if (numQuizzes == 0) {
            return 0;
        } else {
            return totalQuizPoints / numQuizzes;
        }
    }

    public void addQuiz(int quizzPoints) {
        this.totalQuizPoints = totalQuizPoints + quizzPoints;
        this.numQuizzes++;
    }

    static Player getPlayer(Player[] playersArray, String playerName) {
        for (Player player : playersArray) {
            if (player.getName().equals(playerName)) {
                return player;
            }
        }
        return new Player(playerName, 0, 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return numQuizzes == player.numQuizzes && totalQuizPoints == player.totalQuizPoints && Objects.equals(name, player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, numQuizzes, totalQuizPoints);
    }

    static Player[] getTop10(Player... playersArray) {

        if (playersArray.length > 10) {
            Player[] auxPlayerArray = new Player[10];
            Arrays.sort(playersArray);
            for (int i = playersArray.length-1,j=0; j < auxPlayerArray.length; i--,j++) {
                auxPlayerArray[j] = playersArray[i];
            }
            return auxPlayerArray;
        } else {
            Arrays.sort(playersArray, Comparator.reverseOrder());
            return playersArray;
        }
    }

    public static Player minimum(Player... inputArray) {
        int inputArraySize = inputArray.length;

        if (inputArraySize == 0) {
            return null;
        } else {
            Player auxPlayer = inputArray[0];
            for (int i = 0; i < inputArraySize; i++) {
                if (inputArray[i].compareTo(auxPlayer) < 0) {
                    auxPlayer = inputArray[i];
                }
            }
            return auxPlayer;
        }
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        Player p1 = new Player("5:47  - Manuel Torres");
        System.out.println(p1);
    }


}
