package trab1.grupo4;

import trab1.grupo3.Artwork;

public class QuantityRoyalty extends Royalty {

    private final int value;

    private final int numUses;

    private boolean payed;

    public QuantityRoyalty(Artwork artwork,int v, int n) {
        super(artwork);
        this.value = v;
        this.numUses = n;
    }

    public void setPayed(boolean payed) {
        this.payed = payed;
    }

    @Override
    public int getValue() {
        return numUses * value;
    }

    @Override
    public boolean isPayed() {
        return this.payed;
    }

    @Override
    public void pay() throws RoyaltyException {
        if(isPayed())
            throw new RoyaltyException("One royalty cannot be payed twice");
        setPayed(true);
    }
}
