package trab1.grupo4;

public class RoyaltyException extends Exception{

    public RoyaltyException(String message) {
        super(message);
    }
}
