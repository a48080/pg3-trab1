package trab1.grupo4;

import trab1.grupo3.Artwork;

import java.util.Objects;

public abstract class Royalty {

    private Artwork artwork;

    protected Royalty(Artwork artwork) {
        this.artwork = artwork;
    }

    public Artwork getArtwork() {
        return artwork;
    }

    public abstract int getValue();

    public abstract boolean isPayed();

    public abstract void pay() throws RoyaltyException;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Royalty royalty = (Royalty) o;
        return Objects.equals(artwork, royalty.artwork);
    }

    @Override
    public String toString() {
        return "�" + this.getValue() + " --> " + artwork;
    }

    public int removePayed(){
        return 0;
    }
}
