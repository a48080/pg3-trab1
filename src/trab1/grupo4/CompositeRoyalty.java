package trab1.grupo4;

import trab1.grupo3.Artwork;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class CompositeRoyalty extends Royalty {

    List<Royalty> royalties = new ArrayList<>();

    public CompositeRoyalty(Artwork artwork) {
        super(artwork);
    }

    @Override
    public int getValue() {
        int sum = 0;
        for (Royalty r : royalties)
            sum += r.getValue();
        return sum;
    }

    @Override
    public boolean isPayed() {
        if (royalties.isEmpty())
            return false;
        for (Royalty r : royalties) {
            if (!r.isPayed())
                return false;
        }
        return true;
    }

    @Override
    public void pay() {
        if (!isPayed()) {
            for (Royalty r : royalties)
                if (!r.isPayed()) {
                    try {
                        r.pay();
                    } catch (RoyaltyException royaltyException) {
                        throw new RuntimeException("Unexpected Exception", royaltyException);
                    }
                }
        }
    }

    public List<Royalty> getRoyalties(Predicate<Royalty> filter) {
        List<Royalty> result = new ArrayList<>();
        for (Royalty r : royalties) {
            if (filter.test(r)) {
                result.add(r);
            }
        }
        return result;
    }

    public CompositeRoyalty append(Royalty r) throws RoyaltyException {
        if (r.getArtwork().equals(this.getArtwork())) {
            royalties.add(r);
        } else {
            throw new RoyaltyException("Royalties artwork must match!");
        }
        return this;
    }

    public int removePayed() {
        int rSize = royalties.size();
        this.royalties.removeIf(Royalty::isPayed);
        return rSize - royalties.size();
    }

    @Override
    public String toString() {
        return "(MULTI)" + super.toString();
    }

}
