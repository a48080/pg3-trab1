package trab1.grupo3;

public class Song extends Artwork {

    private int seconds;

    public Song(int year, String name, Author author, int seconds) throws ArtworkException {
        super(year, name, author);
        if (seconds < 1)
            throw new ArtworkException("Duration must me greater than 0");
        else
            this.seconds = seconds;
    }

    public int getSeconds() {
        return seconds;
    }

    @Override
    public Artwork getMatch(Artwork a) {
        if (this.equals(a))
            return this;
        return null;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(" [%02d:%02d]",this.getSeconds() / 60,this.getSeconds() % 60);
    }
}
