package trab1.grupo3;

import java.util.Objects;

public abstract class Artwork {

    public final int year;

    private final String name;

    private final Author author;

    protected Artwork(int year, String name, Author author) {
        this.year = year;
        this.name = name;
        this.author = author;
    }

    public final Author getAuthor() {
        return this.author;
    }

    public abstract Artwork getMatch(Artwork a);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o instanceof Artwork) {
            Artwork a = (Artwork) o;
            return year == a.year && Objects.equals(name, a.name) && Objects.equals(author, a.author);
        }
        return false;
    }

    public static Artwork getArtwork(Artwork a, Artwork... aws) {

        for (int i = 0; i < aws.length; i++) {
            Artwork auxArtwork = aws[i].getMatch(a);
            if (auxArtwork != null) {
                return auxArtwork;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.name + " (" + this.getAuthor().getName() + ")";
    }
}
