package trab1.grupo3;

import java.util.Arrays;
import java.util.List;

public class Album extends Artwork {

    protected Song[] songs;

    public Album(String name, Author a, Song... s) {
        super(getLatest(s).year, name, a);
        if (s.length < 2)
            throw new ArtworkException("The album must have at least two songs");
        Arrays.sort(s,new ArtworkComparator());
        this.songs = s;
    }

    @Override
    public Artwork getMatch(Artwork a) {
       return getArtwork(a,songs);
    }

    public static Song getLatest(Song... s) {

        if (s == null || s.length == 0) {
            return null;
        }
        Song songWithLatestYear = s[0];
        for (Song song : s) {
            if (songWithLatestYear.year < song.year)
                songWithLatestYear = song;
        }
        return songWithLatestYear;
    }


    public List<Song> getSongs() {
        return Arrays.asList(this.songs);
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder(year + " - " + super.toString() + "\n");
        for (int i = 0; i < this.songs.length; i++)
            stringBuilder.append("\t").append(i).append(" - ").append(songs[i].toString());
        return stringBuilder.toString();

    }
}
