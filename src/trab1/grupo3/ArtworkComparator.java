package trab1.grupo3;

import java.util.Comparator;

public class ArtworkComparator implements Comparator<Artwork> {
    @Override
    public int compare(Artwork o1, Artwork o2) {
        if(o1.year == o2.year)
            return o1.toString().compareTo(o2.toString());
        else
            return o1.year - o2.year;
    }
}
